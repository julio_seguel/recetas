package com.example.recetas;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText et_nombr;
    private TextView txt_ingred,txt_pasos,txt_titl;
    private Button bt_buscar; //aaafaa

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_ingred = (TextView) findViewById(R.id.txt_ingre);
        this.txt_pasos = (TextView) findViewById(R.id.txt_pasos);
        this.et_nombr = (EditText) findViewById(R.id.et_receta);
        this.bt_buscar = (Button) findViewById(R.id.bt_buscar);
        this.txt_titl = (TextView)findViewById(R.id.txt_title) ;

        bt_buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                String nombre = et_nombr.getText().toString().trim();


                String url = "https://api.edamam.com/search?q="+nombre+"&app_id=09a6f0e3&app_key=e83e1a9d0183847e0706e3d6f833952a";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);


                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String titulo = recipesJSON.getString("label");
                                    txt_titl.setText(titulo);
                                    JSONArray ingredientesARRAY = recipesJSON.getJSONArray("ingredientLines");
                                    String ingredientes = "" ;

                                    for(int i = 0; i<ingredientesARRAY.length();i++){

                                        ingredientes = ingredientes+ ingredientesARRAY.getString(i)+ "\n";


                                    }
                                    txt_ingred.setText(ingredientes);

                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String enerlabel = enerJSON.getString("label");
                                    String enerq = enerJSON.getString("quantity");
                                    String eneru = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fatlabel =fatJSON.getString("label");
                                    String fatq = fatJSON.getString("quantity");
                                    String fatu = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String fasatlabel = fasatJSON.getString("label");
                                    String fasatq = fasatJSON.getString("quantity");
                                    String fasatu = fasatJSON.getString("unit");


                                    String todo = enerlabel + " - " + enerq + " - " + eneru + "\n" +
                                            fatlabel + " - " + fatq + " - " + fatu + "\n" +
                                            fasatlabel + " - " + fasatq + " - " + fasatu + "\n" ;

                                    txt_pasos.setText(todo);

                                    et_nombr.setText("");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });
    }



}
